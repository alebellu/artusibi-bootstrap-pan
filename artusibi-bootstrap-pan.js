/* LICENSE START */
/*
 * ssoup
 * https://github.com/alebellu/ssoup
 *
 * Copyright 2012 Alessandro Bellucci
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://alebellu.github.com/licenses/GPL-LICENSE.txt
 * http://alebellu.github.com/licenses/MIT-LICENSE.txt
 */
/* LICENSE END */

/*
 * start :
 *  - load bootstrap-combined.min.css
 *  - load bootstrap.min.js
 *  - load page_elements.html
 *  - create Edit and Save/Finish buttons
 *  - register click event for the three buttons:
 *    - Edit.click: pan.editMode()
 *    - Save.click: pan.saveAll()
 *    - Finish.click:
 *      - pan.saveAll()
 *      - pan.viewMode()
 *
 * editMode:
 *  - register mouseenter event on .ssoup-container:
 *    - var containerFDiv = this
 *    - var ingredient = $( containerDiv ).data( 'ssoup-ingredient' )
 *    - var context = $( containerDiv ).data( 'ssoup-context' )
 *    - create Edit/Recipes/Select Drawer and Ok/Cancel buttons in a toolbar
 *    - register click event for the buttons:
 *      - Edit.click:
 *        - pan.startEditing( containerDiv )
 *        - var ingredient = $( containerDiv ).data( 'ssoup-ingredient' )
 *        - kitchen.assistant.edit()
 *      - Ok.click:
 *        - pan.stopEditing( containerDiv )
 *        - pan.updateContent( containerDiv )
 *        - var ingredient = $( containerDiv ).data( 'ssoup-ingredient' )
 *        - $( containerDiv ).empty()
 *        - kitchen.assistant.view()
 *      - Cancel.click:
 *        - pan.stopEditing( containerDiv )
 *        - var ingredient = $( containerDiv ).data( 'ssoup-ingredient' )
 *        - $( containerDiv ).empty()
 *        - kitchen.assistant.view()
 *  - register mouseleave event on .ssoup-container:
 *    - hide the toolbar
 *
 * updateContent:
 *  - var updateContentMethod = $( containerDiv ).data( 'ssoup-update-content' )
 *  - var context = $( containerDiv ).data( 'ssoup-context' )
 *  - var currentContent = $( containerDiv ).data( 'ssoup-ingredient' )
 *  - var updatedContent = updateContentMethod( currentContent )
 *  - $( containerDiv ).data( 'ssoup-ingredient', updatedContent )
 *  - var parentContent = $( context.parentContext.containerDiv ).data( 'ssoup-ingredient' )
 *  - parentContent[ context.property ] = updatedContent
 *
 * viewMode:
 *  - hide Save/Finish and show Edit button.
 *  - deregister mouse handlers.
 *
 * startEditing( containerDiv ):
 *  - $( containerDiv ).data( 'mode', 'edit' )
 *  - pan.editingDivs.push( containerDiv )
 *
 * stopEditing( containerDiv ):
 *  - $( containerDiv ).data( 'mode', 'view' )
 *  - remove containerDiv from pan.editingDivs
 *
 * saveAll:
 *  - for each pan.editingDivs
 *    - pan.updateContent( editingDiv )
 *    - $( editingDiv ).data( 'mode', 'view' )
 *    - find the first ancestor where the provenance drawer is specified:
 *      - kitchen.assistant.save()
 *      - kitchen.assistant.view()
 *    - pan.editingDivs = []
 */

/* HEADER START */
var env, requirejs, define;
if (typeof module !== 'undefined' && module.exports) {
    env = "node";
    requirejs = require('requirejs');
    define = requirejs.define;

    requirejs.config({
        baseUrl: __dirname,
        nodeRequire: require
    });

    // require all dependant libraries, so they will be available for the client to request.
    var pjson = require('./package.json');
    for (var library in pjson.dependencies) {
        require(library);
    }
} else {
    env = "browser";
    //requirejs = require;
}

({ define: env === "browser"
        ? define
        : function(A,F) { // nodejs.
            var path = require('path');
            var moduleName = path.basename(__filename, '.js');
            console.log("Loading module " + moduleName);
            module.exports = F.apply(null, A.map(require));
            global.registerUrlContext("/" + moduleName + ".js", {
                "static": __dirname,
                defaultPage: moduleName + ".js"
            });
            global.registerUrlContext("/" + moduleName, {
                "static": __dirname
            });
        }
}).
/* HEADER END */

define(['jquery', 'artusi-kitchen-tools'], function($, tools) {

    var drdf = tools.drdf;
    var drff = tools.drff;

    var bootstrap = function(context) {
        var pan = this;

        pan.context = context;

        pan.mode = 'view';
        pan.editingDivs = [];

        pan.recipesPreferencesIngredients = {};
        pan.recipesPreferencesTypes = {};
    };

    bootstrap.prototype.init = function() {
        var dr = $.Deferred();

        dr.resolve();

        return dr.promise();
    };

    bootstrap.prototype.start = function() {
        var dr = $.Deferred();
        var pan = this;
        var kitchen = pan.context.kitchen;

        // Load custom, Twitter Bootstrap and Chosen (for rich combo box) CSS and JS
        this.cssBootstrap = tools.getStylesheet("//netdna.bootstrapcdn.com/twitter-bootstrap/2.1.1/css/bootstrap-combined.min.css");
        this.cssChosen = tools.getStylesheet("/artusibi-bootstrap-pan/lib/chosen.min.css");
        this.css = tools.getStylesheet("/artusibi-bootstrap-pan/artusibi-bootstrap-pan.css");

        tools.getScripts(
            [
                "//netdna.bootstrapcdn.com/twitter-bootstrap/2.1.1/js/bootstrap.min.js",
                "/artusibi-bootstrap-pan/lib/chosen.jquery.min.js"
            ]).done(function() {
            // load menu
            // kitchen.assistant.getResource({
            //    resourceIRI: "/artusibi-bootstrap-pan/page_elements.html",
            //    postProcess: "jQuery"
            $.ajax({
                url: "/artusibi-bootstrap-pan/page_elements.html"
            }).done(function(pageElemsText) {
                var pageElems = $(pageElemsText);
                $('body').prepend(pageElems.find('#navbar'))
                    .append(pageElems.find('#modals'))
                    .append(pageElems.find('#recipes'))
                    .append(pageElems.find('#newInstance'))
                    .append(pageElems.find('#deleteInstance'))
                    .append(pageElems.find('#footer'));

                $('#doneEditModeModal').hide();
                $('#viewCacheModal').hide();

                $('#recipes').hide();

                $('#newInstance').hide();

                $('#deleteInstance').hide();

                // hide the command buttons until they are used
                $('.menuCommand').hide();

                $('#editCommand').on('click', function() {
                    pan.editMode();
                });
                $('#saveCommand').on('click', function() {
                    pan.saveAll();
                });
                $('#doneCommand').on('click', function() {
                    if (pan.editingDivs.length > 0) {
                        $('#doneEditModeModal').modal('show');

                        $('#doneEditModeModalDiscardCommand').on('click', function() {
                            // close any open editor
                            $.each(pan.editingDivs, function(index, editingDiv) {
                                var ingredient = $(editingDiv).data('ssoup-ingredient-original');
                                if (ingredient) {
                                    // revert to original content
                                    $(editingDiv).data('ssoup-ingredient', ingredient);
                                    $(editingDiv).removeData('ssoup-ingredient-original');
                                }
                                else {
                                    ingredient = $(editingDiv).data('ssoup-ingredient');
                                }
                                var context = $(editingDiv).data('ssoup-context');
                                $(editingDiv).empty();
                                $(editingDiv).data('mode', 'view');

                                // reset recipes preferences
                                pan.recipesPreferencesIngredients = {};
                                pan.recipesPreferencesTypes = {};

                                kitchen.assistant.view({
                                    "sp:ingredient": ingredient,
                                    "sp:context": context
                                });
                            });
                            pan.editingDivs = [];
                            $('#doneEditModeModal').modal('hide');
                            pan.viewMode();
                        });
                        $('#doneEditModeModalSaveCommand').on('click', function() {
                            pan.saveAll();
                            $('#doneEditModeModal').modal('hide');
                            pan.viewMode();
                        });
                    }
                    else {
                        pan.viewMode();
                    }

                    return false;
                });

                dr.resolve();
            }).fail(drff(dr));
        });

        return dr.promise();
    };

    bootstrap.prototype.editMode = function() {
        var pan = this;
        var kitchen = pan.context.kitchen;

        pan.mode = 'edit';

        $('#editCommand').hide();
        $('#saveCommand').show();
        $('#doneCommand').show();

        // show kitchen menu
        pan.showKitchenMenu();

        // Activate relative positioning for ssoup-container for later showing toolbar overlay, for current and future container divs.
        //$( '.ssoup-container' ).css( 'position', 'relative' );
        $('<style>.ssoup-container { position: relative; }</style>').appendTo('head');

        // Register hover handler to show/hide edit and configure icons above view/edit divs, now and in the future.
        //$(document).on('mouseenter', '.ssoup-container', function() {
        $(document).on('click', '.ssoup-container', function() {
            var containerDiv = this;

            if ($(containerDiv).children('.btn-toolbar').length > 0) {
                return;
            }

            $(containerDiv).css('outline', 'black dotted thin');
            $(containerDiv).css('outline-top', 'black dotted 20px');

            var ingredient = $(containerDiv).data('ssoup-ingredient');
            var context = $(containerDiv).data('ssoup-context');

            var editCommand = $('<a class="btn" href="#"><i class="icon-pencil"></i></a>');
            var recipesCommand = $('<a class="recipes-cmd btn" href="#"><i class="icon-align-justify"></i></a>');
            var newInstanceCommand = $('<a class="new-instance-cmd btn" href="#"><i class="icon-plus"></i></a>');
            var deleteCommand = $('<a class="delete-instance-cmd btn" href="#"><i class="icon-trash"></i></a>');
            var editPreviewCommand = $('<a class="btn" href="#"><i class="icon-eye-open"></i></a>');
            var editCancelCommand = $('<a class="btn" href="#"><i class="icon-remove"></i></a>');

            var viewCommandGroup = $('<div class="btn-group"></div>');
            viewCommandGroup.append(editCommand);
            viewCommandGroup.append(recipesCommand);
            viewCommandGroup.append(newInstanceCommand);
            if (context.parentContext) { // root ingredient cannot be deleted
                viewCommandGroup.append(deleteCommand);
            }

            var editCommandGroup = $('<div class="btn-group"></div>');
            editCommandGroup.append(editPreviewCommand);
            // TODO: Cancel command not implemented yet: waiting for full history and undo/redo
            //editCommandGroup.append( editCancelCommand );

            /* version with top menu: takes too much space
		var toolbar = $( '<div class="btn-toolbar">' );
		toolbar.append( viewCommandGroup );

		var menubar = $( '<div class="navbar navbar-fixed-top" style="position: absolute; left: 0; top: 0;">' );
		var menubarinner = $( '<div class="navbar-inner">' );
		menubar.append( menubarinner );
		menubarinner.append( toolbar );
		$( containerDiv ).append( menubar );*/

            var toolbar = $('<div class="btn-toolbar" style="position: absolute; left: 0; top: -35;">');
            if ($(containerDiv).data('mode') == 'edit') {
                toolbar.empty().append(editCommandGroup);
            }
            else {
                toolbar.empty().append(viewCommandGroup);
            }

            /* Drawer combo */
            var prov = ingredient["sw:provenance"];
            if (!prov) {
                ingredient["sw:provenance"] = {};
                prov = ingredient["sw:provenance"];
            }
            var selectedDrawerPath = prov["prov:atLocation"];

            var drawerSelect = $('<div class="btn-group">');
            var selectedDrawerButton = $('<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret">');
            if (selectedDrawerPath) {
                var selectedDrawer = kitchen.drawers[selectedDrawerPath];
                if (selectedDrawer) {
                    selectedDrawerButton.append('<img src="' + selectedDrawer.getIcon32Url() + '" width="32"> ' + selectedDrawerPath).append('<span class="caret"></span>');
                }
            }
            else {
                selectedDrawerButton.append(' Select a drawer');
            }
            drawerSelect.append(selectedDrawerButton);

            var dropdownMenu = $('<ul class="dropdown-menu">').appendTo(drawerSelect);

            $.each(kitchen.drawers, function(drawerIRI, drawer) {
                dropdownMenu.append('<li><img src="' + drawer.getIcon32Url() + '" width="32"> ' + drawerIRI + '</li>')
            });
            // toolbar.append(drawerSelect);

            $(containerDiv).append(toolbar);

            editCommand.on('click', function() {
                pan.startEditing(containerDiv);

                toolbar.empty().append(editCommandGroup);

                var ingredient = $(containerDiv).data('ssoup-ingredient');
                kitchen.assistant.edit({
                    "sp:ingredient": ingredient,
                    "sp:context": context
                });

                return false;
            });
            editPreviewCommand.on('click', function() {
                pan.stopEditing(containerDiv);

                toolbar.empty().append(viewCommandGroup);

                pan.updateContent(containerDiv);

                var ingredient = $(containerDiv).data('ssoup-ingredient');

                $(containerDiv).css('outline', 'none').empty();
                kitchen.assistant.view({
                    "sp:ingredient": ingredient,
                    "sp:context": context
                });

                return false;
            });
            editCancelCommand.on('click', function(e) {
                pan.stopEditing(containerDiv);

                toolbar.empty().append(viewCommandGroup);

                var ingredient = $(containerDiv).data('ssoup-ingredient');
                $(containerDiv).css('outline', 'none').empty();
                kitchen.assistant.view({
                    "sp:ingredient": ingredient,
                    "sp:context": context
                });

                e.preventDefault();

                return false;
            });

            var recipesPopoverContent = $($('#recipes')[0].outerHTML).attr('id', $(containerDiv).attr('id') + '-recipes-popover').css('display', 'block')[0].outerHTML;
            recipesCommand.popover({
                html: true,
                trigger: 'manual',
                placement: 'bottom',
                title: 'Recipes',
                content: recipesPopoverContent
            }).on('click', function(e) {
                // if popover is visible
                var popover = pan.getRecipesPopover(containerDiv);
                if (popover.is(':visible')) {
                    //recipesCommand.popover( 'hide' );
                    popover.remove();

                    // reset to default width for next display time
                    popover.css('width', '236px');

                    // remove popover footer.
                    popover.find('.popover-footer').remove();
                }
                else {
                    // first close all other recipes, deklete and new instance popovers
                    //$('.recipes-cmd').popover('hide'); does not work properly
                    $('.recipes-popover').closest('.popover').remove();
                    $('.delete-instance-popover').closest('.popover').remove();
                    $('.new-instance-popover').closest('.popover').remove();

                    var ingredient = $(containerDiv).data('ssoup-ingredient');
                    var context = $(containerDiv).data('ssoup-context');

                    recipesCommand.popover('show');
                    var popover = pan.getRecipesPopover(containerDiv);

                    // set popover parent to containerDiv to avoid mouseenter mouseleave handlers problems
                    var popoverOffset = popover.offset();
                    popover.appendTo($(containerDiv));
                    popover.offset(popoverOffset);
                    /*popover.on( 'mouseenter', function() {
    					pan.mouseOnPopover = true;
    				} ).on( 'mouseleave', function() {
    					pan.mouseOnPopover = false;
    				} );*/

                    var recipesCategoriesSelect = popover.find('.recipesCategoriesSelect');
                    kitchen.assistant.listRecipesCategories().done(function(recipesCategories) {
                        $.each(recipesCategories, function(index, recipeCategory) {
                            recipesCategoriesSelect.append($('<option value="' + recipeCategory + '">' + recipeCategory + '</option>'));
                        });

                        recipesCategoriesSelect.chosen();
                        recipesCategoriesSelect.on('change', function() {
                            var categories = popover.find('.recipesCategoriesSelect').val();
    
                            // when categories change,empty and collapse details if expanded, and remove recipe use select and popover footer
                            $('.recipeDetails').css({'min-height': '0px'}).empty();
                            popover.animate({
                                width: '236px'
                            });
                            popover.find('.recipeUse').remove();
                            popover.find('.popover-footer').remove();
    
                            popover.find('.recipesList').find('.nav-list .recipe').remove();
    
                            if (!categories || categories.length == 0) {
                                return;
                            }
    
                            kitchen.assistant.listRecipes({
                                "sp:ingredient": ingredient,
                                "sp:categories": categories,
                                "sp:searchAncestors": true,
                            }).done(function(recipes) {
                                if (recipes && recipes.length > 0) {
                                    $.each(recipes, function(index, recipe) {
                                        if (recipe.value && recipe.value.length > 0) {
                                            var recipeId = recipe.value[0].impl["@id"];
                                            var recipeName = recipe.value[0].impl["sr:recipeName"];
                                            var recipeItem = $('<a href="#">' + recipeName + '</a>');
    
                                            recipeItem.on('click', function(e) {
                                                popover.find('.recipesList').find('.nav-list .recipe.active').removeClass('active');
                                                $(this).parent().addClass('active');

                                                // change the position of the popover arrow from percentage to absolute to avoid moving when resizing the popover
                                                var popoverArrow = popover.find('.arrow');
                                                popoverArrow.css('left', popoverArrow.offset().left - popover.offset().left);

                                                var recipeUseSelect = $('<div class="recipeUse controls" style="max-width: 150px; position: relative">');
                                                var recipeUseForCurrentIngredientOption = $('<div class="recipeUseOption currentIngredient star_checkbox"><div class="pull-left star_0" /><div class="pull-left recipeUseOptionDescription">Preferred recipe for current ingredient</div></div>');
                                                var recipeUseForTypeOption = $('<div class="recipeUseOption type star_checkbox"><div class="pull-left star_0" /><div class="pull-left recipeUseOptionDescription">Preferred recipe for all ingredients of type <b>' + ingredient["@type"] + '</b></div></div>');
                                                recipeUseSelect.append(recipeUseForCurrentIngredientOption);
                                                recipeUseSelect.append(recipeUseForTypeOption);
                                                popover.find('.recipeUse').remove();
                                                popover.find('.recipeSelect').append(recipeUseSelect);
                                                $('.star_checkbox').on('click', function() {
                                                    $(this).find('.star_0').removeClass('star_0').addClass('star_1_temp');
                                                    $(this).find('.star_1').removeClass('star_1').addClass('star_0');
                                                    $(this).find('.star_1_temp').removeClass('star_1_temp').addClass('star_1');
                                                });

                                                pan.setRecipePreferenceIngredient(containerDiv.id, categories, recipeId);

                                                popover.animate({
                                                    width: '400px'
                                                }, {
                                                    duration: 200,
                                                    queue: false,
                                                    complete: function() {
                                                        popover.find('.recipeDetails').empty().css({
                                                            'width': '200px',
                                                            'min-height': '100px',
                                                            'border-left': '1px solid gray',
                                                            'margin-left': '5px',
                                                            'padding-left': '5px',
                                                            'float': 'left'
                                                        }).append('<b>' + recipeName + '</b>');

                                                        var executeRecipeButton = $('<a class="btn btn-primary pull-right" href="#"><i class="icon-play icon-white"></i> Execute recipe</a>');
                                                        executeRecipeButton.on('click', function(e) {
                                                            kitchen.assistant.executeRecipe({
                                                                recipe: {
                                                                    origin: recipe.origin,
                                                                    recipe: recipe.value[0]
                                                                },
                                                                context: context,
                                                                inputParameters: {
                                                                    "sp:ingredient": ingredient
                                                                }
                                                            }).done(function() {
                                                                kitchen.assistant.view({
                                                                    "sp:ingredient": ingredient,
                                                                    "sp:context": context
                                                                });
                                                            }).fail(function(error) {
                                                                alert(error);
                                                            });
        
                                                            e.preventDefault();
        
                                                            return false;
                                                        });
        
                                                        popover.find('.popover-footer').remove();
                                                        popover.find('.popover-inner').append($('<div class="popover-footer" style="margin: 10px">').append(executeRecipeButton));
                                                    }
                                                });
                                                e.preventDefault();
                                            });
                                            var recipeLi = $('<li class="recipe">').append(recipeItem);
                                            popover.find('.recipesList').find('.nav-list').append(recipeLi);
                                        }
                                    });
                                }
                            });
                        });
                    });
                }
                e.preventDefault();
            });

            var newInstancePopoverContent = $($('#newInstance')[0].outerHTML).attr('id', $(containerDiv).attr('id') + '-new-instance-popover').css('display', 'block')[0].outerHTML;
            newInstanceCommand.popover({
                html: true,
                trigger: 'manual',
                placement: 'bottom',
                title: 'Create new instance',
                content: newInstancePopoverContent
            }).on('click', function(e) {
                // if popover is visible
                var popover = pan.getNewInstancePopover(containerDiv);
                if (popover.is(':visible')) {
                    popover.remove();
                }
                else {
                    // first close all other new instance, delete and recipes popovers
                    $('.new-instance-popover').closest('.popover').remove();
                    $('.delete-instance-popover').closest('.popover').remove();
                    $('.recipes-popover').closest('.popover').remove();

                    var ingredient = $(containerDiv).data('ssoup-ingredient');
                    var context = $(containerDiv).data('ssoup-context');

                    newInstanceCommand.popover('show');
                    var popover = pan.getNewInstancePopover(containerDiv);

                    // set popover parent to containerDiv to avoid mouseenter mouseleave handlers problems
                    var popoverOffset = popover.offset();
                    popover.appendTo($(containerDiv));
                    popover.offset(popoverOffset);

                    // retrieve properties for all types of ingredient and add them to the combo
                    var typePropertiesSelect = popover.find('.typePropertiesSelect');
                    var typesSelect = popover.find('.typesSelect');
                    var createButton = popover.find('.btn-primary');
                    kitchen.assistant.getTypes({
                        "sp:ingredient": ingredient
                    }).done(function(types) {
                        if (types && types.length > 0) {
                            $.each(types, function(i, type) {
                                kitchen.assistant.getTypeProperties({
                                    "sp:type": type
                                }).done(function(typeProperties) {
                                    if (typeProperties && typeProperties.length > 0) {
                                        $.each(typeProperties[0], function(k, typeProperty) {
                                            var opt = $('<option>' + k + '</option>');
                                            opt.data('property', typeProperty);
                                            typePropertiesSelect.append(opt);
                                        });
                                    }
                                });
                            });
                        }
                    });
                    typePropertiesSelect.on('change', function() {
                        createButton.attr('disabled', true);

                        var property = typePropertiesSelect.find(":selected").data('property');
                        typesSelect.empty();
                        if (property) {
                            var propertyRange = property["rdfs:range"]["@id"];
                            if (propertyRange == 'rdf:List') {
                                propertyRange = property["ssoup:rangeListItemsType"]["@id"];
                            }
                            var opt = $('<option>' + propertyRange + '</option>');
                            opt.val(propertyRange);
                            typesSelect.append(opt);
                        }
                    });
                    typesSelect.on('change', function() {
                        var property = typePropertiesSelect.find(":selected").data('property');
                        var range = typePropertiesSelect.find(":selected").val();
                        if (range) {
                            createButton.removeAttr('disabled');
                        }
                    });
                    createButton.on('click', function() {
                        var property = typePropertiesSelect.find(":selected").data('property');
                        var rangeType = typesSelect.find(":selected").val();
                        if (property && rangeType) {
                            kitchen.assistant.getTypeProperties({
                                "sp:type": rangeType
                            }).done(function(rangeProperties) {
                                var range = {
                                    "@type": rangeType
                                };

                                if (rangeProperties && rangeProperties.length > 0) {
                                    $.each(rangeProperties[0], function(k, rangeProperty) {
                                        if (rangeProperty["ssoup:defaultValue"]) {
                                            range[rangeProperty["@id"]] = rangeProperty["ssoup:defaultValue"];
                                        }
                                    });
                                }

                                var propertyRange = property["rdfs:range"]["@id"];
                                if (propertyRange == 'rdf:List') {
                                    if (!ingredient[property["@id"]]) {
                                        ingredient[property["@id"]] = [];
                                    }

                                    ingredient[property["@id"]].push(range);
                                } else {
                                    ingredient[property["@id"]] = range;
                                }

                                if (context.containerDiv) {
                                    if ($.inArray(context.containerDiv, pan.editingDivs) == -1) {
                                        pan.editingDivs.push(context.containerDiv);
                                    }
                                    pan.propagateContentToAncestors(context.containerDiv);
                                    $(context.containerDiv).empty();
                                }
                                kitchen.assistant.view({
                                    "sp:ingredient": ingredient,
                                    "sp:context": context
                                });
                            });
                        }
                    });
                }

                e.preventDefault();
            });

            var deleteInstancePopoverContent = $($('#deleteInstance')[0].outerHTML).attr('id', $(containerDiv).attr('id') + '-delete-instance-popover').css('display', 'block')[0].outerHTML;
            deleteCommand.popover({
                html: true,
                trigger: 'manual',
                placement: 'bottom',
                title: 'Delete object',
                content: deleteInstancePopoverContent
            }).on('click', function(e) {
                // if popover is visible
                var popover = pan.getDeleteInstancePopover(containerDiv);
                if (popover.is(':visible')) {
                    popover.remove();
                }
                else {
                    // first close all other new instance, delete and recipes popovers
                    $('.new-instance-popover').closest('.popover').remove();
                    $('.delete-instance-popover').closest('.popover').remove();
                    $('.recipes-popover').closest('.popover').remove();

                    var ingredient = $(containerDiv).data('ssoup-ingredient');
                    var context = $(containerDiv).data('ssoup-context');

                    deleteCommand.popover('show');
                    var popover = pan.getDeleteInstancePopover(containerDiv);

                    // set popover parent to containerDiv to avoid mouseenter mouseleave handlers problems
                    var popoverOffset = popover.offset();
                    popover.appendTo($(containerDiv));
                    popover.offset(popoverOffset);

                    var deleteButton = popover.find('.btn-primary');
                    deleteButton.on('click', function() {
                        if (context && context.parentContext) {
                            var parentContext = context.parentContext;
                            var parentIngredient = $(parentContext.containerDiv).data('ssoup-ingredient');

                            if (parentIngredient[context.property]) {
                                if (context.propertyIndex !== undefined) {
                                    parentIngredient[context.property].splice(context.propertyIndex, 1);
                                } else {
                                    delete parentIngredient[context.property];
                                }
                            }

                            if ($.inArray(parentContext.containerDiv, pan.editingDivs) == -1) {
                                pan.editingDivs.push(parentContext.containerDiv);
                            }
                            pan.propagateContentToAncestors(parentContext.containerDiv);
                            $(parentContext.containerDiv).empty();

                            kitchen.assistant.view({
                                "sp:ingredient": parentIngredient,
                                "sp:context": parentContext
                            });
                        }
                    });
                }

                e.preventDefault();
            });

        });
        $(document).on('mouseleave', '.ssoup-container', function() {
            var containerDiv = this;

            if (!pan.mouseOnPopover &&
                !pan.getRecipesPopover(containerDiv).is(':visible') &&
                !pan.getNewInstancePopover(containerDiv).is(':visible') &&
                !pan.getDeleteInstancePopover(containerDiv).is(':visible')
            ) {
                $(containerDiv).children('.navbar').remove();
                $(containerDiv).children('.btn-toolbar').remove();
                $(containerDiv).css('outline', 'none');
            }
        });
    };

    bootstrap.prototype.getRecipesPopover = function(containerDiv) {
        var popoverContentId = $(containerDiv).attr('id') + '-recipes-popover';
        return $('[id="' + popoverContentId + '"]').closest('.popover');
    };

    bootstrap.prototype.getNewInstancePopover = function(containerDiv) {
        var popoverContentId = $(containerDiv).attr('id') + '-new-instance-popover';
        return $('[id="' + popoverContentId + '"]').closest('.popover');
    };

    bootstrap.prototype.getDeleteInstancePopover = function(containerDiv) {
        var popoverContentId = $(containerDiv).attr('id') + '-delete-instance-popover';
        return $('[id="' + popoverContentId + '"]').closest('.popover');
    };

    bootstrap.prototype.updateContent = function(containerDiv) {
        var pan = this;
        var updateContentMethod = $(containerDiv).data('ssoup-update-content');
        if (updateContentMethod) {
            var context = $(containerDiv).data('ssoup-context');

            // backup original data
            var currentContent = $(containerDiv).data('ssoup-ingredient');
            if (!$(containerDiv).data('ssoup-ingredient-original')) {
                // Deep copy
                var originalIngredient = jQuery.extend(true, {}, currentContent);
                $(containerDiv).data('ssoup-ingredient-original', originalIngredient);
            }

            // update node data
            var updatedContent = updateContentMethod(currentContent);
            $(containerDiv).data('ssoup-ingredient', updatedContent);
        }

        this.propagateContentToAncestors(containerDiv);
    };

    bootstrap.prototype.propagateContentToAncestors = function(containerDiv) {
        var context = $(containerDiv).data('ssoup-context');
        var content = $(containerDiv).data('ssoup-ingredient');

        while (context.parentContext && context.property) {
            var parentContent = $(context.parentContext.containerDiv).data('ssoup-ingredient');

            if (context.propertyIndex != undefined) {
                parentContent[context.property][context.propertyIndex] = content;
            }
            else {
                parentContent[context.property] = content;
            }
            context = context.parentContext;
            content = parentContent;
        }
    };

    bootstrap.prototype.viewMode = function() {
        var pan = this;

        pan.mode = 'view';

        $('#editCommand').show();
        $('#saveCommand').hide();
        $('#doneCommand').hide();

        // hide kitchen menu
        $('#kitchenCommand').hide();

        // Deregister mouseenter/mouseleave handlers to hide icons above view/edit divs
        $('.ssoup-container').off('click');
        $('.ssoup-container').off('mouseenter');
        $('.ssoup-container').off('mouseleave');
        $('.ssoup-container').css('outline', 'none');
    };

    bootstrap.prototype.startEditing = function(containerDiv) {
        var pan = this;

        $(containerDiv).data('mode', 'edit');
        if ($.inArray(containerDiv, pan.editingDivs) == -1) {
            pan.editingDivs.push(containerDiv);
        }
    };

    bootstrap.prototype.stopEditing = function(containerDiv) {
        var pan = this;

        $(containerDiv).data('mode', 'view');

        /* Never keep a div out of the pan.editingDivs array until closing the editing session.
	var i = $.inArray( containerDiv, pan.editingDivs );
	if ( i >= 0 ) {
		pan.editingDivs.splice(i, 1);
	}*/
        /*for(var i = 0 ; i < pan.editingDivs.length ; i++) {
		if(containerDiv.isSameNode(pan.editingDivs[i])) {
			pan.editingDivs.splice(i, 1);
			break;
		}
	}*/
    };

    bootstrap.prototype.saveAll = function() {
        var dr = $.Deferred();
        var pan = this;
        var drs = [];
        var kitchen = pan.context.kitchen;

        // save recipes preferences
        pan.saveRecipesPreferences().done(function() {
            var divsToSave = [];
            // close any open editor
            $.each(pan.editingDivs, function(index, editingDiv) {
                pan.updateContent(editingDiv);

                $(editingDiv).data('mode', 'view');
                var context = $(editingDiv).data('ssoup-context');
                // find the first ancestor where the provenance drawer is specified.
                var ingredient = $(editingDiv).data('ssoup-ingredient');
                if ($(editingDiv).data('save-id') === true) {
                    ingredient["@id"] = $(editingDiv).attr('id');
                    pan.propagateContentToAncestors(editingDiv);
                }
                var parentDiv;
                while (!(ingredient && ingredient["sw:provenance"] && ingredient["sw:provenance"]["prov:atLocation"])) {
                    context = context.parentContext;
                    parentDiv = context.containerDiv;
                    ingredient = $(parentDiv).data('ssoup-ingredient');
                }
                if ($.inArray(parentDiv, divsToSave) == -1) {
                    divsToSave.push(parentDiv);
                }
            });

            $.each(divsToSave, function(index, divToSave) {
                var ingredient = $(divToSave).data('ssoup-ingredient');
                var context = $(divToSave).data('ssoup-context');
                var drl = kitchen.assistant.save({
                    "sp:ingredient": ingredient,
                }).done(function() {
                    if (context.containerDiv) {
                        $(context.containerDiv).empty();
                    }
                    kitchen.assistant.view({
                        "sp:ingredient": ingredient,
                        "sp:context": context
                    });
                });

                drs.push(drl);
            });

            // wait for all save operations to complete
            $.when.apply($, drs).done(drdf(dr)).fail(drff(dr));

            pan.editingDivs = [];
        }).fail(drff(dr));

        return dr.promise();
    };

    bootstrap.prototype.getSpecificAttributes = function() {
        return {
            "sw:MainVisualizationPan": true
        };
    };

    bootstrap.prototype.getContext = function() {};

    bootstrap.prototype.stop = function() {
        // disable custom, Twitter Bootstrap and Chosen CSS
        this.css.attr('disabled', 'true');
        this.cssBootstrap.attr('disabled', 'true');
        this.cssChosen.attr('disabled', 'true');
    };

    bootstrap.prototype.beforeRecipeCall = function(recipePanParams, context, inputParameters) {
        var dr = $.Deferred();
        var pan = this;
        var kitchen = pan.context.kitchen;

        if (context.parent && !context.containerDiv) {
            // create container div for the viewer/editor
            context.containerDiv = $('<div class="ssoup-container"><span class="ssoup-loading">Loading...</span></div>');
            var ingredient = inputParameters["sp:ingredient"];
            // and assign it the id of the ingredient or a pseudo-unique id if it is not present
            if (ingredient["@id"]) {
                context.containerDiv.attr('id', ingredient["@id"]);
            } else {
                context.containerDiv.attr('id', kitchen.tools.ssoupId());
            }
            context.containerDiv.data('ssoup-ingredient', ingredient);
            context.containerDiv.data('ssoup-context', context);
            context.parent.find('.ssoup-loading').remove();
            context.parent.append(context.containerDiv);
        }

        // Set deferred as resolved.
        dr.resolve();
        return dr;
    };

    bootstrap.prototype.showAuthInfo = function(authInfo) {
        var dr = $.Deferred();
        var pan = this;

        $.each(authInfo, function(index, auth) {
            var anchor = pan.buildAuthAnchor(index, auth);

            $('#authMenu').append(
            $('<li id="auth-li-' + index + '">').attr('data-node', auth.origin).append(anchor));
        });
        $('#authCommand').show();
        dr.resolve();

        return dr.promise();
    };

    bootstrap.prototype.buildAuthAnchor = function(index, auth) {
        var pan = this;

        return $('<a tabindex="-1" href="' +
                    (auth.value["sa:isAuthenticated"] ? auth.value["sa:authLogoutURL"] : auth.value["sa:authLoginURL"]) +
                '">' + (auth.value["sa:isAuthenticated"] ? "Logout " + auth.value["sa:userName"] : "Login to " + auth.origin) + '</a>');
        /*.on('click', function() {
            kitchen.worktop.publish({
                message: {
                    "@type": "sm:task",
                    "sm:taskType": (auth.value[0].authorized ? "sm:logout" : "sm:login"),
                    "sm:taskParameters": {
                        "sm:addServiceRecipes": true
                    }
                },
                recipients: [
                    [auth.origin]
                ],
                modality: "swt:FirstAvailableResponse",
                postProcess: "notEmptyAddOrigin"
            }).done(function(auths) {
                // Update menu item
                var anchor = pan.buildAuthAnchor(index, auths[0]);
                $('#auth-li-' + index).empty().append(anchor);
                pan.updateNodeServiceRecipes(auths[0].origin, auths[0].value[0].serviceRecipes);
            });
        });*/
    };

    bootstrap.prototype.showServiceRecipes = function(serviceRecipes) {
        var dr = $.Deferred();
        var pan = this;

        $.each(serviceRecipes, function(index, nodeServiceRecipes) {
            pan.updateNodeServiceRecipes(nodeServiceRecipes.origin, nodeServiceRecipes.value);
        });
        dr.resolve();

        return dr.promise();
    };

    bootstrap.prototype.updateNodeServiceRecipes = function(nodeURI, serviceRecipes) {
        var pan = this;
        var kitchen = pan.context.kitchen;
        var li, ul;

        li = $('#serviceRecipesMenu').find('.nodeServiceRecipes[data-node="' + nodeURI + '"]');
        if (!serviceRecipes || serviceRecipes.length == 0) {
            if (li) {
                li.remove();
                if ($('#serviceRecipesMenu').children().length == 0) {
                    $('#serviceRecipesCommand').hide();
                    //$('#editCommand').hide();
                    $('#editCommand').show(); // for the moment show edit command anyway.
                }
            }
            return;
        }

        if (li.length > 0) {
            ul = li.find('ul.dropdown-menu');
        }
        else {
            ul = $('<ul class="dropdown-menu">');
            li = $('<li class="dropdown-submenu nodeServiceRecipes" data-node="' + nodeURI + '">').append($('<a tabindex="-1" href="#">' + nodeURI + '</a>')).append(ul);
            $('#serviceRecipesMenu').append(li);
        }

        ul.empty();
        $.each(serviceRecipes, function(index, serviceRecipeWithOrigin) {
            if ($.isArray(serviceRecipeWithOrigin.value) && serviceRecipeWithOrigin.value.length > 0) {
                var serviceRecipe = serviceRecipeWithOrigin.value[0];
                var anchor = $('<a tabindex="-1" href="#">' + serviceRecipe.impl["rdfs:label"] + '</a>');
                anchor.on('click', function() {
                    kitchen.worktop.publish({
                        message: {
                            "@type": "sm:task",
                            "sm:taskType": "sm:" + serviceRecipe.impl["sr:recipeName"],
                            "sm:taskParameters": {}
                        },
                        recipients: [
                            [nodeURI]
                        ],
                        modality: "swt:FirstAvailableResponse"
                    });
                });

                ul.append(
                $('<li>').attr('data-node', nodeURI).append(anchor));
            }
        });
        $('#serviceRecipesCommand').show();
        $('#editCommand').show();
    };

    bootstrap.prototype.showKitchenMenu = function() {
        var dr = $.Deferred();
        var pan = this;
        var kitchen = pan.context.kitchen;

        var ul = $('#kitchenMenu');
        ul.empty();

        var anchor = $('<a tabindex="-1" href="#">View worktop cache</a>');
        anchor.on('click', function() {
            var cacheString = escape(JSON.stringify(kitchen.worktop.cache));
            $('#viewCacheTextArea').val(cacheString);
            $('#viewCacheModal').modal('show');
        });

        ul.append($('<li>').append(anchor));

        $('#kitchenCommand').show();

        dr.resolve();

        return dr.promise();
    };

    bootstrap.prototype.setRecipePreferenceIngredient = function(ingredientContainerDivId, categories, recipeIRI) {
        var pan = this;

        var recipesCategories = categories.sort();
        if (!pan.recipesPreferencesIngredients[ingredientContainerDivId]) {
            pan.recipesPreferencesIngredients[ingredientContainerDivId] = {};
        }
        pan.recipesPreferencesIngredients[ingredientContainerDivId][recipesCategories] = recipeIRI;
    };

    bootstrap.prototype.setRecipePreferenceType = function(type, categories, recipeIRI) {
        var pan = this;

        var recipesCategories = categories.sort();
        pan.recipesPreferencesTypes[type][recipesCategories] = recipeIRI;
    };

    bootstrap.prototype.saveRecipesPreferences = function() {
        var dr = $.Deferred();
        var pan = this;
        var kitchen = pan.context.kitchen;

        var drs = [];
        $.each(pan.recipesPreferencesIngredients, function(ingredientContainerDivId, recipePreferenceIngredient) {
            var ingredientContainerDiv = $('[id="' + ingredientContainerDivId + '"]');
            var ingredient = ingredientContainerDiv.data( 'ssoup-ingredient' );
            var context = ingredientContainerDiv.data('ssoup-context');
            // if the ingredient has no id then add one and put the ingredient in the list of edited ones.
            if (!ingredient["@id"]) {
                ingredientContainerDiv.data('save-id', true);
                if ($.inArray(ingredientContainerDiv, pan.editingDivs) == -1) {
                    // if this div is not scheduled for saving, add it to list of edited divs in order to save the id
                    pan.editingDivs.push(ingredientContainerDiv);
                }
            }

            $.each(recipePreferenceIngredient, function(categories, recipeIRI) {
                var drl = kitchen.assistant.savePreferredRecipe({
                    recipesCategories: categories,
                    ingredientIRI: ingredientContainerDiv.attr('id'),
                    recipeIRI: recipeIRI
                });
                drs.push(drl);
            });
        });
        $.each(pan.recipesPreferencesTypes, function(type, recipePreferenceType) {
            $.each(recipePreferenceType, function(categories, recipeIRI) {
                var drl = kitchen.assistant.savePreferredRecipe({
                    recipesCategories: categories,
                    type: type,
                    recipeIRI: recipeIRI
                });
                drs.push(drl);
            });
        });

        // wait for all save operations to complete
        $.when.apply($, drs).done(drdf(dr)).fail(drff(dr));

        return dr.promise();
    };

    return bootstrap;
});
